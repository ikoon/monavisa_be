/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.exampleBehavior = {
    attach: function (context, settings) {
      //alert("I'm alive!");
      if($('.user-not-logged-in #header-wrapper' ).length == 1) {
        $('.user-not-logged-in #header-wrapper' ).clone().prependTo( '#header-wrapper' );
      }
      $('.file--mime-application-pdf a').each(function(){$(this).attr({ target: "_blank" });});
      $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        var objectSelect = $(".view-mode-full");
        var objectPosition = objectSelect.offset().top -650;
        if( scroll > objectPosition ) {
          $('body').addClass('fixed-header-now');
        } else {
          $('body').removeClass('fixed-header-now');
        }
      });

    }
  };

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.cookieSettings = {
    attach: function (context, settings) {
      $('span[data-action="cookie_settings"]', context).click(function() {
        $('.eu-cookie-withdraw-tab').click();
      });
      $(function(){
        $('#sliding-popup').css({bottom: - $('#sliding-popup').height()});
      });
    }
  };

    /**
     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.slides = {
        attach: function (context, settings) {
            $(context).find('.field-name-field-slides.swiper-container').once('ifSlider').each(function (){
                var slides = new Swiper (this, {
                    autoplay: true,
                    loop: true,
                    effect: 'fade',
                    navigation: {
                      nextEl: $(this).parent().find('.swiper-custom-button-next'),
                      prevEl: $(this).parent().find('.swiper-custom-button-prev')
                    },
                    pagination: {
                      el: '.swiper-custom-pagination',
                      clickable: true,
                      renderBullet: function (index, className) {
                        var counter = index + 1;
                        if(index <= 9) {
                          counter = '0' +  (index + 1);
                        }
                        return '<span class="' + className + '">' + counter + '</span>';
                      },
                    },
                });
            });

          $(context).find('.field-name-field-quote.swiper-container').once('ifQuoteSlider').each(function (){
            var slides = new Swiper (this, {
              loop: true,
              effect: 'slide',
              autoplay: {
                delay: 6000
              },
              speed: 1200
            });
          });

          $(context).find('.paragraph-realisations.swiper-container').once('ifRealisationSlider').each(function (){
            var slides = new Swiper (this, {
              autoplay: true,
              loop: true,
              effect: 'slide',
              navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
              }
            });
          });

          $(context).find('.realisation-image-slider.swiper-container').once('ifSlider').each(function (){
            var slides = new Swiper (this, {
              autoplay: false,
              loop: false,
              spaceBetween: 10,
              slidesPerView: 'auto',
              effect: 'slide',
              navigation: {
                nextEl: '.swiper-button-next.img-rea',
                prevEl: '.swiper-button-prev.img-rea'
              }
            });
          });

          $(context).find('.realisation-image-slider-sector.swiper-container').once('ifSlider').each(function (){
            var slides = new Swiper (this, {
              autoplay: false,
              loop: false,
              spaceBetween: 10,
              slidesPerView: 'auto',
              effect: 'slide',
              navigation: {
                nextEl: '.swiper-button-next.img-rea',
                prevEl: '.swiper-button-prev.img-rea'
              }
            });
          });

          $(context).find('.finisher-slider.swiper-container').once('ifSlider').each(function (){
            var slides = new Swiper (this, {
              autoplay: false,
              loop: false,
              slidesPerView: '5',
              spaceBetween: 10,
              effect: 'slide',
              navigation: {
                nextEl: $(this).parent().find('.swiper-button-next.finishbutton'),
                prevEl: $(this).parent().find('.swiper-button-prev.finishbutton'),
              },
              breakpoints: {

                768: {
                  slidesPerView: 1,
                  spaceBetween: 10
                },
                1200: {
                  slidesPerView: 3,
                  spaceBetween: 10
                }
              }
            });
          });

          $(context).find('.cert-slider.swiper-container').once('ifSlider').each(function (){
            var slides = new Swiper (this, {
              autoplay: {
                delay: 5000
              },
              speed:1000,
              loop: true,
              slidesPerView: '5',
              spaceBetween: 10,
              effect: 'slide',
              breakpoints: {
                768: {
                  slidesPerView: 1,
                  spaceBetween: 10
                },
                1200: {
                  slidesPerView: 3,
                  spaceBetween: 10
                }
              }
            });
          });

          $(context).find('.images-slider').once('ifSlider').each(function (){
            var slides = new Swiper (this, {
              autoplay: false,
              loop: false,
              spaceBetween: 10,
              slidesPerView: 'auto',
              effect: 'slide',
              navigation: {
                nextEl: $(this).parent().find('.slider-next'),
                prevEl: $(this).parent().find('.slider-prev')
              }
            });
          });
        }
    };

    /**
     * offCanvas behaviors
     */
    Drupal.behaviors.offCanvas = {
        attach: function (context, settings) {
            $(context).find('#offCanvasRightOverlap').once('ifRightCanvas').each(function () {
                $menuButton = $('#menuButton');
                $(this).on("opened.zf.offcanvas", function(e){
                    $menuButton.addClass('is-active');
                });
                $(this).on("closed.zf.offcanvas", function(e){
                    $menuButton.removeClass('is-active');
                });
            });
        }
    };


  Drupal.behaviors.imagesLoaded = {
    attach: function (context, settings) {
      $('#lightgallery').lightGallery();
      $('.grid').imagesLoaded(function () {
        $('.grid').masonry({
          itemSelector: '.grid-item',
          percentPosition: false,
          gutter: 10,
          fitWidth: false
        });
      });

      $(".scroll-toggle").click(function() {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
      });


        var distance = $('#main-wrapper').offset().top;
        console.log(distance);
      $(window).scroll(function () {
        if ($(this).scrollTop() >= distance) {
          $('.scroll-toggle').addClass('in-view');
        } else {
          $('.scroll-toggle').removeClass('in-view');
        }
      });


    }
  };

  Drupal.behaviors.popUp = {
    attach: function (context, settings) {
      $(context).find('#block-popup').once('ifPopUp').each(function () {
        var cookiePopUpCheck = Cookies.get("popUp-check");

        function checkCookie() {
          cookiePopUpCheck = Cookies.get("popUp-check", 1, {expires: 2});
        }
        if (!cookiePopUpCheck) {
          $('#block-popup').foundation('open');
        }

        $('#close-pop-up').on('click', function () {
          $("#block-popup").on("closed.zf.reveal", function (e) {
            checkCookie();
          });
          $('#block-popup').foundation('close');
        });
      });
    }
  }

})(jQuery, Drupal);
